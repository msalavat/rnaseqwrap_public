#!/bin/bash
#This is the script for the analysis
#SGE flags
#$ -N RNASeqWRAP        ##TBC1
#$ -cwd
#$ -l h_rt=24:00:00
#$ -l h_vmem=20G
## -r yes
#$ -notify
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 6
#$ -V
#$ -t 1-25
## -tc 5

#The correct folder structure for the analysis of choice. 
RUN_PATH=/YOURFOLDERSTRUCTURE/
vCPU=6

mkdir -p $RUN_PATH/Reports
mkdir -p $RUN_PATH/dump
mkdir -p $RUN_PATH/index
REPORTS=$RUN_PATH/Reports 

module load sratoolkit/2.8.2-1
module load fastqc/0.11.7
module load trimmomatic/0.36
module load kallisto/0.44.0
module load anaconda
module load pigz/2.3.3

#The python environment for parallel sratools was precompiled according to the instructions: 
#https://github.com/rvalieris/parallel-fastq-dump

# This env has python 3.7
source activate parallel_sra_install
# download SRA files from file with list of  SRA ids
sra_id=$(awk "NR==${SGE_TASK_ID}" $HOME/workplace/rnaseqwrap/sra_list.txt)
#cd ${RUN_PATH}/dump/
parallel-fastq-dump --threads 40 --sra-id ${sra_id} --gzip --split-files -O ${RUN_PATH}/dump/ --tmpdir ${RUN_PATH}/dump/
#cd ${RUN_PATH}
source deactivate

# run fastqc on raw data
fastqc -t $((${vCPU}/2)) ${RUN_PATH}/dump/${sra_id}_1.fastq.gz -o $REPORTS &\
fastqc -t $((${vCPU}/2)) ${RUN_PATH}/dump/${sra_id}_2.fastq.gz -o $REPORTS

# trim reads with trimmomatic
NAME=$(basename -s _R1.fq.gz ${RUN_PATH}/dump/${sra_id}_1.fq.gz)
java -jar trimmomatic/0.36/trimmomatic-0.36.jar PE -threads ${vCPU} \
-basein ${RUN_PATH}/dump/${sra_id}_1.fastq.gz \
-baseout ${RUN_PATH}/dump/${sra_id}_trimmed.fq.gz \
ILLUMINACLIP:/gpfs/igmmfs01/software/pkg/el7/apps/trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10:1:TRUE SLIDINGWINDOW:5:20 MINLEN:50

# remove raw data
rm -f ${RUN_PATH}/dump/${sra_id}_1.fastq.gz ${RUN_PATH}/dump/${sra_id}_2.fastq.gz

# run fastqc on trimmed data

fastqc -t $((${vCPU}/2)) ${RUN_PATH}/dump/${sra_id}_trimmed_1P.fq.gz -o $REPORTS &\
fastqc -t $((${vCPU}/2)) ${RUN_PATH}/dump/${sra_id}_trimmed_2P.fq.gz -o $REPORTS

# remove the unpaired reads
rm -f ${RUN_PATH}/dump/${sra_id}_trimmed_1U.fq.gz ${RUN_PATH}/dump/${sra_id}_trimmed_2U.fq.gz


if [[ -f "${RUN_PATH}/index/Ovis_aries.Oar_v3.1.cdna.all.fa.gz" && -f "${RUN_PATH}/index/GCF_002742125.1_Oar_rambouillet_v1.0_rna_from_genomic2.fna.gz" ]];then
	echo "Index exists!"
else
	function download_indexes() {
	wget "ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/742/125/GCF_002742125.1_Oar_rambouillet_v1.0/GCF_002742125.1_Oar_rambouillet_v1.0_rna_from_genomic.fna.gz" \
	-P ${RUN_PATH}/index/
    	wget "ftp://ftp.ensembl.org/pub/release-98/fasta/ovis_aries/cdna/Ovis_aries.Oar_v3.1.cdna.all.fa.gz" \
	-P ${RUN_PATH}/index/
	}
	download_indexes
fi;
	
# checking the presence of indexes
if [[ -f "${RUN_PATH}/index/Oar_v3.1.idx" && -f "${RUN_PATH}/index/Oar_Ramb_v1.0.idx" ]];then
		echo "All set!"
	else
		function build_indexes(){
   		kallisto index -i ${RUN_PATH}/index/Oar_v3.1.idx ${RUN_PATH}/index/Ovis_aries.Oar_v3.1.cdna.all.fa.gz
   		kallisto index -i ${RUN_PATH}/index/Oar_Ramb_v1.0.idx ${RUN_PATH}/index/GCF_002742125.1_Oar_rambouillet_v1.0_rna_from_genomic.fna.gz
		}
		build_indexes
fi;

# Kalisto quantification
Tfile="${RUN_PATH}/dump/${sra_id}_trimmed_1P.fq.gz"
mkdir -p $RUN_PATH/${sra_id}
mkdir -p $RUN_PATH/${sra_id}/Kallisto_out_OAR
mkdir -p $RUN_PATH/${sra_id}/Kallisto_out_RAMB
kallisto quant --bias -t ${vCPU} -i $RUN_PATH/index/Oar_v3.1.idx -o $RUN_PATH/${sra_id}/Kallisto_out_OAR <(zcat ${Tfile}) <(zcat ${Tfile/_1P.fq.gz/_2P.fq.gz})
kallisto quant --bias -t ${vCPU} -i $RUN_PATH/index/Oar_Ramb_v1.0.idx -o $RUN_PATH/${sra_id}/Kallisto_out_RAMB <(zcat ${Tfile}) <(zcat ${Tfile/_1P.fq.gz/_2P.fq.gz})


# remove the paired reads
rm -f ${RUN_PATH}/dump/${sra_id}_trimmed_1P.fq.gz ${RUN_PATH}/dump/${sra_id}_trimmed_2P.fq.gz
