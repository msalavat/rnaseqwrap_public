This pipeline is designed for on fly analysis of the RNA-Seq datasets directly from SRA

1. Creating local Kallisto indices for specie of choice
2. Pulling the data from SRA using SRATools
3. Local fastQC check prior to trimming
4. Trimming using Trimmomatic
5. Local fastQC check post trimming prior to mapping
6. Pseudoalignment and quantification with Kallisto (--pseudobam)
7. Storing TPM tsv file along with multi QC of the pre/post trim fastqc.zip reports
8. Visualising TPM outputs using R scripts (optional)



DOI: https://doi.org/10.1101/2020.07.06.189480

Publication title: 

Global analysis of transcription start sites in the new ovine reference genome (Oar rambouillet v1.0)

Mazdak Salavati1\*, Alex Caulton2,3\*, Richard Clark4, Iveta Gazova1,5, Tim P. Smith6, Kim C. Worley7, Noelle E. Cockett8, Alan L. Archibald1, Shannon Clarke2, Brenda Murdoch9, Emily L. Clark1 and The Ovine FAANG Consortium

1The Roslin Institute and Royal (Dick) School of Veterinary Studies, University of Edinburgh, Edinburgh, UK
2AgResearch, Invermay Agricultural Centre, Mosgiel, New Zealand
3University of Otago, Dunedin, New Zealand
4Genetics Core, Clinical Research Facility, University of Edinburgh, Edinburgh, UK
5Institute for Genetics and Molecular Medicine, University of Edinburgh, Edinburgh, UK
6USDA, ARS, USMARC, Clay Center, Nebraska, USA
7Baylor College of Medicine, Houston, Texas, USA
8Utah State University, Logan, Utah, USA
9University of Idaho, Moscow, Idaho, USA

*These two authors contributed equally to the work
Corresponding author: emily.clark@roslin.ed.ac.uk
